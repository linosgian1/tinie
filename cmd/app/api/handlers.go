package api

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"

	"github.com/gorilla/mux"
)

func shortenURL(c *Context, w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("%v", err), http.StatusInternalServerError)
		return
	}
	r.Body.Close()

	vars := mux.Vars(r)
	url := vars["url"]

	id := newID()
	c.Urls[id] = url
	w.Write([]byte(fmt.Sprintf("%s/%s", "http://localhost:8080", id)))
}

func redirToUrl(c *Context, w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("%v", err), http.StatusInternalServerError)
		return
	}
	r.Body.Close()

	vars := mux.Vars(r)
	id := vars["id"]

	http.Redirect(w, r, c.Urls[id], http.StatusFound)
}

func lookupID(c *Context, w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("%v", err), http.StatusInternalServerError)
		return
	}
	r.Body.Close()

	vars := mux.Vars(r)
	id := vars["id"]

	w.Write([]byte(c.Urls[id]))
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func newID() string {
	b := make([]rune, 10)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
