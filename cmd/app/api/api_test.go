package api

import (
	"log"
	"net/http/httptest"
	"os"
	"testing"

	klog "github.com/go-kit/kit/log"
	"github.com/linosgian/tinie/cmd/app/config"
	"github.com/stretchr/testify/assert"
)

var (
	conf *config.Config
	err  error
)

func TestDoubleSlashInURL(t *testing.T) {
	assert := assert.New(t)
	ap := setupTestContext(t)
	req := httptest.NewRequest("POST", "/api/v1/shorten/https://koko.lala", nil)
	w := httptest.NewRecorder()
	ap.Router.ServeHTTP(w, req)

	assert.Equal(200, w.Code)
}

func setupTestContext(t *testing.T) *API {
	ap := &API{}
	logger := klog.NewLogfmtLogger(klog.NewSyncWriter(os.Stdout))
	ap.Init(conf, &logger)
	return ap
}

func init() {
	conf, err = config.LoadConfig("../config-test.json")
	if err != nil {
		log.Fatalf("Failed to load config file: %v\n", err)
	}
}
