package api

import (
	"context"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	klog "github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"github.com/linosgian/tinie/cmd/app/config"
)

type Context struct {
	Urls   map[string]string
	Config *config.Config
	Logger *klog.Logger
}

type API struct {
	Context *Context
	Router  *mux.Router
}

func (a *API) Init(c *config.Config, logger *klog.Logger) {
	a.Context = &Context{
		Config: c,
		Logger: logger,
		Urls:   make(map[string]string),
	}
	// Allow double forward slashes in paths.
	// This is needed so we mux can parse the following:
	// `/api/v1/shorten/https://foo.bar`
	a.Router = mux.NewRouter().SkipClean(true)
	a.setupRoutes()
	rand.Seed(time.Now().UnixNano())
}

func (a *API) Run(listenAddr, configPath string) {
	sigCh := make(chan os.Signal, 1)
	srv := &http.Server{
		Addr:    listenAddr,
		Handler: a.Router,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen failed: %v\n", err)
		}
	}()

	signal.Notify(sigCh, os.Interrupt,
		syscall.SIGINT, syscall.SIGTERM,
	)
	for {
		s := <-sigCh
		switch s {
		case syscall.SIGINT, syscall.SIGTERM, os.Interrupt:
			ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
			defer func() {
				cancel()
			}()
			if err := srv.Shutdown(ctx); err != nil {
				log.Fatalf("Server shutdown failed: %v\n", err)
			}
			return
		}
	}
}

func (a *API) setupRoutes() {
	a.Post("/api/v1/shorten/{url:.*}", a.handleRequest(shortenURL))
	a.Get("/api/v1/lookup/{id}", a.handleRequest(lookupID))
	a.Get("/{id}", a.handleRequest(redirToUrl))
}

func (a *API) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

func (a *API) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

type RequestHandlerFunction func(c *Context, w http.ResponseWriter, r *http.Request)

func (a *API) handleRequest(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(a.Context, w, r)
	}
}
