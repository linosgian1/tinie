package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/go-playground/validator"
)

type Config struct {
	ListenAddress string `validate:"required" json:"listen-address"`
}

func LoadConfig(configPath string) (*Config, error) {
	buf, err := ioutil.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("could not open config file path: %v", err)
	}
	var conf Config
	if err = json.Unmarshal(buf, &conf); err != nil {
		return nil, fmt.Errorf("could not unmarshal config as json: %v", err)
	}

	validate := validator.New()
	if err = validate.Struct(conf); err != nil {
		return nil, fmt.Errorf("invalid configuration: %v", err)
	}
	return &conf, nil
}
