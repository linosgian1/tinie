package main

import (
	"fmt"
	"log"
	"os"
	"path"

	klog "github.com/go-kit/kit/log"
	"github.com/go-stack/stack"
	"github.com/linosgian/tinie/cmd/app/api"
	"github.com/linosgian/tinie/cmd/app/config"
	"github.com/urfave/cli"
)

var version string

func main() {
	app := cli.NewApp()
	app.Name = "tinie"
	app.Usage = "tinie URL shortener HTTP server"
	app.Version = version

	app.Commands = []cli.Command{
		{
			Name: "server",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:  "config",
					Value: path.Join(os.Getenv("PWD"), "config.json.example"),
					Usage: "File path to configuration file",
				},
				&cli.StringFlag{
					Name:  "listen-address",
					Value: "127.0.0.1:8080",
					Usage: "Colon separated IP and port to bind to, e.g. 0.0.0.0:8080",
				},
			},
			Usage:  "Start the HTTP server",
			Action: runServer,
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatalf("Could not start HTTP server: %v\n", err)
	}

}

func runServer(c *cli.Context) error {
	configPath := c.String("config")
	conf, err := config.LoadConfig(configPath)
	if err != nil {
		return cli.NewExitError(fmt.Sprintf("Failed to load config file: %v\n", err), 1)
	}
	if c.String("listen-address") != "" {
		conf.ListenAddress = c.String("listen-address")
	}
	api := &api.API{}
	w := klog.NewSyncWriter(os.Stderr)
	logger := klog.NewLogfmtLogger(w)
	file := func() interface{} {
		return fmt.Sprintf("%s", stack.Caller(3))
	}
	lineno := func() interface{} {
		return fmt.Sprintf("%s", stack.Caller(3))
	}
	function := func() interface{} {
		return fmt.Sprintf("%n", stack.Caller(3))
	}
	logger = klog.With(logger, "ts", klog.DefaultTimestampUTC,
		"app", "api",
		"file", klog.Valuer(file),
		"lineno", klog.Valuer(lineno),
		"function", klog.Valuer(function))
	api.Init(conf, &logger)
	logger.Log("action", "startup", "address", conf.ListenAddress)
	api.Run(conf.ListenAddress, configPath)
	return nil
}
