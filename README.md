# Tinie

This is a URL-shortener HTTP service to rule them all.

## Usage

Spin up the HTTP server

```
make && cd cmd/app && ./tinie server
```

Then shorten all the things like so:

```
curl -XPOST http://localhost:8080/api/v1/shorten/https://foo.bar
```

You can even follow the URL that you'll get back in your browser.

## Development

In order to run the tests run:

```
make check
```

This will also look for race conditions with go test -race
