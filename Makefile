VERSION=0.0.1

all: fmt build

fmt:
	@if [ -n "$(shell gofmt -l .)" ]; then \
		echo "Source code needs reformatting. Run 'go fmt' manually "; \
		false; \
	fi

build:
	${MAKE} -C cmd/app build VERSION=$(VERSION)

check: build
	go test -race -p 1 ./...
	${MAKE} clean

clean:
	go clean ./...
	${MAKE} -C cmd/app clean

distclean: clean
	go clean -i -cache -testcache

vet:
	go vet ./...

lint:
	golint ./...
